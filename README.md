# Flysystem - Google Cloud Storage

Flysystem Google Cloud Storage provides a Google Cloud Storage plugin
for Flysystem.
This plugin allows you to replace the local file system with [Google
Storage](https://console.cloud.google.com/storage).
Flysystem GCS can be used as the default file system or can be configured
for each file/image field separately.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/flysystem_gcs).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/flysystem_gcs).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

Upload the Service Account JSON file to your site by following these steps:
1. [Create a project in GCP](https://cloud.google.com/resource-manager/docs/creating-managing-projects).
2. Go to the [storage browser](https://console.cloud.google.com/storage) and [create a bucket](https://cloud.google.com/storage/docs/creating-buckets).
3. Create a Service Account and get private key:
    - Go to the [Service Accounts page](https://console.cloud.google.com/iam-admin/serviceaccounts).
    - Create Service Account with Storage Admin role.
    - Create a new key for the Service Account (key type - JSON).
    - Download the JSON file to a private folder in your site.

This module requires the following dependencies:
- [Flysystem](https://www.drupal.org/project/flysystem)
- [Flysystem Adapter for Google Cloud Storage](https://github.com/Superbalist/flysystem-google-cloud-storage)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Stream wrappers are configured in settings.php (see the Flysystem README.md).

### Example configuration
```php
$settings['flysystem'] = [
  'cloud-storage' => [
    'driver' => 'gcs',
    'config' => [
      'bucket' => 'example',
      'keyFilePath' => '/serviceaccount.json',
      'projectId' => 'google-project-id',
      // Time in seconds before a request to GCS times out.
      'requestTimeout' => 5, 
      // More options: https://googlecloudplatform.github.io/google-cloud-php/#/docs/google-cloud/v0.46.0/storage/storageclient?method=__construct.
      // Optional local configuration; see https://github.com/Superbalist/flysystem-google-cloud-storage#google-storage-specifics.
      '_localConfig' => [
        'prefix' => 'extra-folder/another-folder/',
        // Change part of URLs from https://storage.googleapis.com/[bucket_name]/extra-folder/another-folder/ to https://cname/.
        'uri' => 'https://cname',
      ],
    ],
    'cache' => true, // Cache filesystem metadata.
  ],
];
```

After this, make sure to apply the following configuration changes to your Drupal installation:
- Change the default download method for File system at `/admin/config/media/file-system`.
- Change the upload destination for existing fields.
